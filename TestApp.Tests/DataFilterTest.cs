﻿using System;
using System.Collections.Generic;
using TestApp.ClassLibrary;
using Xunit;

namespace TestApp.Tests
{
    public class DataFilterTest
    {
        [Fact]
        public void MedianFilterData_W3_Test()
        {
            var inputData = new byte[] { 2, 80, 6, 3 };
            List<byte> result = DataFilter.MedianFilter(inputData, 3);

            Assert.Equal(new byte[] { 2, 6, 6, 3 }, result.ToArray());
        }

        [Theory]
        [InlineData(5)]
        [InlineData(15)]
        [InlineData(1000001)]
        public void MedianFilterDataWindowTest(int window)
        {
            var inputData = new byte[] { 2, 80, 6, 3 };
            List<byte> result = DataFilter.MedianFilter(inputData, window);

            Assert.Equal(new byte[] { 2, 3, 3, 3 }, result.ToArray());
        }

        [Fact]
        public void MedianFilterDataEvenWindowSizeTest()
        {
            Assert.Throws<ArgumentException>(() => {
                var inputData = new byte[] { 2, 80, 6, 3 };
                List<byte> result = DataFilter.MedianFilter(inputData, 4);
            });
        }
    }
}
