﻿using System;
using System.Collections.Generic;
using TestApp.ClassLibrary;
using Xunit;

namespace TestApp.Tests
{
    public class DataProcessTest
    {
        [Fact]
        public void GetProcessedDataInputType1Test()
        {
            string input = "﻿1 255 250\r\n2 6 8 7\r\n3 6 7 8 7 5 5\r\n4 6 7 8 7 5 5";
            var pd = new DataProcess(input);
            List<ProcessedData> result = pd.GetProcessedData(3);

            Assert.Equal((byte)1, result[0].DataType);
            Assert.Equal((int)250, result[0].ResultValue);
            Assert.Equal(new byte[] { 255, 250 }, result[0].FilteredValues);
            Assert.Equal((byte)2, result[1].DataType);
            Assert.Equal((int)81, result[1].ResultValue);
            Assert.Equal(new byte[] { 6, 7, 7}, result[1].FilteredValues);
            Assert.Equal((byte)3, result[2].DataType);
            Assert.Equal((int)8, result[2].ResultValue);
            Assert.Equal(new byte[] { 6, 7, 7, 7, 5, 5 }, result[2].FilteredValues);
            Assert.Equal((byte)4, result[3].DataType);
            Assert.Equal((int)5, result[3].ResultValue);
            Assert.Equal(new byte[] { 6, 7, 7, 7, 5, 5 }, result[3].FilteredValues);
        }

        [Theory]
        [InlineData("﻿wrong data 345")]
        [InlineData("7 5 6")]
        public void GetProcessedDataWrongInputTest(string input)
        {
            Assert.Throws<ArgumentException>(()=> {
                var pd = new DataProcess(input);
                List<ProcessedData> result = pd.GetProcessedData(3);
            });
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("﻿wrong data 345")]
        [InlineData("7 5 6")]
        public void DataIsValidFalseTest(string input)
        {
            var pd = new DataProcess(input);
            bool result = pd.InputDataIsValid();
            Assert.True(!result);
        }

        [Theory]
        [InlineData("1 5 6")]
        [InlineData("3 6 7 8 7 5 5")]
        [InlineData("﻿1 5 6\r\n2 8 6 5\r\n3 6 7 8 7 8 9 123\r\n1 5 5\r\n1 6 5\r\n2 7 7 7\r\n3 6 7 8 7 5 5\r\n4 6 7 8 7 5 5")]
        public void DataIsValidTrueTest(string input)
        {
            var pd = new DataProcess(input);
            bool result = pd.InputDataIsValid();
            Assert.True(result);
        }
    }
}
