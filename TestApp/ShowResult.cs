﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestApp.ClassLibrary;

namespace TestApp
{
    public static class ShowResult
    {
        public static void Show(List<ProcessedData> resultsList)
        {
            Console.WriteLine($"DataType |Result |Filtered ");
            foreach (ProcessedData result in resultsList)
            {
                string spaces = String.Empty.PadLeft("Result".Length - result.ResultValue.ToString().Length, ' ');

                string filtered = string.Concat(result.FilteredValues.Select(x => x.ToString() + " "));
                Console.WriteLine($"{result.DataType}        |{result.ResultValue}{spaces} |{filtered}");
            }
        }
    }
}
