﻿using System;
using System.Collections.Generic;
using System.IO;
using TestApp.ClassLibrary;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (FileStream fileStream = File.OpenRead(@"..\..\..\InputData.txt"))
            {
                byte[] array = new byte[fileStream.Length];
                fileStream.Read(array, 0, array.Length);
                string textFromFile = System.Text.Encoding.Default.GetString(array);

                var processData = new DataProcess(textFromFile);
                var medianFilterWindows = new int[] { 3, 5, 11, 100001 };
                if (processData.InputDataIsValid())
                {
                    foreach (var window in medianFilterWindows)
                    {
                        List<ProcessedData> resultsList = processData.GetProcessedData(window);
                        Console.WriteLine($"Window: {window}");
                        ShowResult.Show(resultsList);
                    }
                }
            }
        }
    }
}
