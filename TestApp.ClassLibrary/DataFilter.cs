﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestApp.ClassLibrary
{
    public static class DataFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valuesCollection"></param>
        /// <param name="windowSize">Must be uneven</param>
        /// <returns></returns>
        public static List<byte> MedianFilter(IEnumerable<byte> valuesCollection, int windowSize)
        {
            if (valuesCollection?.Count() == 0)
                return new List<byte>();

            if (windowSize % 2 == 0)
                throw new ArgumentException("Window size must be uneven");

            int halfWindow = windowSize / 2;
            var valuesList = valuesCollection.ToList();
            byte firstValue = valuesList[0];
            byte lastValue = valuesList[valuesList.Count() - 1];

            for (int i = 0; i < halfWindow; i++)
            {
                valuesList.Insert(0, firstValue);
                valuesList.Add(lastValue);
            }

            var filteredCollection = new List<byte>();

            for (int i = halfWindow; i < valuesList.Count() - halfWindow; i++)
            {
                List<byte> windowLine = valuesList.GetRange(i - halfWindow, windowSize);
                windowLine.Sort();
                byte filteredValue = windowLine[halfWindow];
                filteredCollection.Add(filteredValue);
            }

            return filteredCollection;
        }
    }
}
