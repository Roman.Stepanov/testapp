﻿using System.Collections.Generic;

namespace TestApp.ClassLibrary
{
    public struct ProcessedData
    {
        public byte DataType;
        public int ResultValue;
        public IEnumerable<byte> FilteredValues;
    }
}
