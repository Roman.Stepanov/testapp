﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TestApp.ClassLibrary
{
    public class DataProcess
    {
        private readonly string _input;
        private bool? _inputDataIsValid;
        private const byte DIV = 255;

        public DataProcess(string input)
        {
            _input = input;
        }

        public List<ProcessedData> GetProcessedData(int medianFilterWindow)
        {
            if (_inputDataIsValid == null)
                InputDataIsValid();

            if (_inputDataIsValid == false)
                throw new ArgumentException("Input data is not valid");

            var dataList = new List<ProcessedData>();

            MatchCollection textLinesCollection = Regex.Matches(_input + "\r\n", @"(.*?)\r\n");

            foreach (Match textLine in textLinesCollection)
            {
                string valuesTextLine = textLine.Groups[1].Value;
                MatchCollection valuesCollection = Regex.Matches(valuesTextLine, @"\d+");

                var valuesList = valuesCollection
                    .Select(m => Convert.ToByte(m.Groups[0].Value))
                    .ToList();

                if (valuesList.Count() > 0)
                {
                    IEnumerable<byte> values = valuesList.TakeLast(valuesList.Count() - 1);
                    byte dataType = valuesList[0];
 
                    var data = new ProcessedData
                    {
                        DataType = dataType,
                        ResultValue = GetResult(dataType, values),
                        FilteredValues = DataFilter.MedianFilter(values, medianFilterWindow),
                    };

                    dataList.Add(data);
                }
            }

            return dataList;
        }

        public bool InputDataIsValid()
        {
            if (_inputDataIsValid != null)
                return (bool)_inputDataIsValid;

            if (string.IsNullOrEmpty(_input))
                return false;
            var ex1 = Regex.IsMatch(_input, @"^([1-4]{1})", RegexOptions.Multiline);
            var ex2 = Regex.IsMatch(_input, @"([0-9\s])", RegexOptions.Multiline);
            bool inputIsValid = ex1 && ex2;
            _inputDataIsValid = inputIsValid;
            return inputIsValid;
        }

        private int GetResult(byte dataType, IEnumerable<byte> valuesCollection)
        {
            switch (dataType)
            {
                case 1:
                    {
                        var sum = valuesCollection.Sum(x => x);
                        return sum % DIV;
                    }
                case 2:
                    {
                        int multipl = valuesCollection.Aggregate(1, (x, y) => x * y);
                        return multipl % DIV;
                    }
                case 3:
                    {
                        return valuesCollection.Max(x => x);
                    }
                case 4:
                    {
                        return valuesCollection.Min(x => x);
                    }
            }
            throw new ArgumentException();
        }
    }
}
